package wolf.lesson20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    final int MENU_ALPHA = 1;
    final int MENU_Scale = 2;

    TextView tvTest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTest = (TextView) findViewById(R.id.tv_test);
        registerForContextMenu(tvTest);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.add(0, MENU_ALPHA, 0, "alpha");
        menu.add(0, MENU_Scale, 0, "scale");

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Animation anim = null;

        switch (item.getItemId()){
            case MENU_ALPHA:
                anim = AnimationUtils.loadAnimation(this, R.anim.myalpha);
                break;
            case MENU_Scale:
                anim = AnimationUtils.loadAnimation(this, R.anim.myscale);
                break;
        }
        tvTest.startAnimation(anim);
        return super.onContextItemSelected(item);
    }
}
