package wolf.lesson19;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText enum1, enum2;
    private Button btnSum, btnVid, btnMno, btnDil;
    private TextView res;
    private String oper = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

    }

    private void init(){
        enum1 = (EditText) findViewById(R.id.enum1);
        enum2 = (EditText) findViewById(R.id.enum2);

        btnSum = (Button) findViewById(R.id.btn_sum);
        btnSum.setOnClickListener(this);

        btnVid = (Button) findViewById(R.id.btn_vid);
        btnVid.setOnClickListener(this);

        btnMno = (Button) findViewById(R.id.btn_mno);
        btnMno.setOnClickListener(this);

        btnDil = (Button) findViewById(R.id.btn_dil);
        btnDil.setOnClickListener(this);

        res = (TextView) findViewById(R.id.tv_res);
    }

    private void clean(){
        enum1.getText().clear();
        enum2.setText("0");
    }

    @Override
    public void onClick(View v) {

        if (TextUtils.isEmpty(enum1.getText().toString()) || TextUtils.isEmpty(enum2.getText().toString()))
            Toast.makeText(this, "Empty poly", Toast.LENGTH_LONG).show();

        float enume1 = Float.parseFloat(enum1.getText().toString());
        float enume2 = Float.parseFloat(enum2.getText().toString());
        float result = 0;
        switch (v.getId()) {
            case R.id.btn_sum:
                oper = "+";
                result = enume1 + enume2;
                clean();
                break;
            case R.id.btn_vid:
                oper = "-";
                result = enume1 - enume2;
                clean();
                break;
            case R.id.btn_mno:
                oper = "*";
                result = enume1 * enume2;
                clean();
                break;
            case R.id.btn_dil:
                oper = "/";
                if (enume1 == 0 || enume2 == 0)
                    Toast.makeText(this, " / na 0 failed", Toast.LENGTH_LONG).show();

                result = enume1 / enume2;
                clean();
                break;
        }

        res.setText(enume1 + " " + oper + " " + enume2 + " = " + result);
    }
}
