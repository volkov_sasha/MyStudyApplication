package wolf.lesson12;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvOut;
    Button btnOK;
    Button btnCancel;

    public static final String TAG = "myLog";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "search element in View");
        tvOut = (TextView) findViewById(R.id.tv_Out);
        btnOK = (Button) findViewById(R.id.btnOK);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        Log.d(TAG, "listener Button");
        btnOK.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        case R.id.btnOK:
            Toast.makeText(this, "Click OK", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Button OK");
            tvOut.setText("Click OK");
            break;
         case R.id.btnCancel:
             Toast.makeText(this, "Click Cancel", Toast.LENGTH_SHORT).show();
             Log.d(TAG, "Button Cansel");
             tvOut.setText("Click Cancel");
             break;

        }
    }
}
